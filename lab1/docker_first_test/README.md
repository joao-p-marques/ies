Example docker container image for PostgreSQL server

# Docker cheatsheet 

## List Docker CLI commands
docker
docker container --help

## Display Docker version and info
docker --version
docker version
docker info

## Execute Docker image
docker run hello-world

## List Docker images
docker image ls

## List Docker containers (running, all, all in quiet mode)
docker container ls
docker container ls --all
docker container ls -aq

# Run

Use `docker ps` to see wich port is being used

Run ```sudo psql -h localhost -p <PORT> -d docker -U docker --password``` using the right port (32768)
Authenticate with password defined in the Dockerfile ('docker')

Connection estabilshed
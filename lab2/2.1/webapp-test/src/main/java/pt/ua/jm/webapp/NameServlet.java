package pt.ua.jm.webapp;
 
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
 
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
@WebServlet(name = "NameServlet", urlPatterns = {"/NameServlet"})
public class NameServlet extends HttpServlet {
 
    private static final long serialVersionUID = -1915463532411657451L;
 
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException
    {
         
        // Map<String,String> data = getData();

        String username = request.getParameter("username");
        if (username == null)
            username = "Guest";

        String password = null;
         
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            // Write some content
            out.println("<html>");
            out.println("<head>");
            out.println("<title>NameServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h2>Hello " + username + "</h2>");
            out.println("<h2>The time right now is : " + new Date() + "</h2>");
            // out.println("<h2>Password: " + password.toString() + "</h2>"); // gera NullPointerException
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }
     
    // //This method will access some external system as database to get user name, and his personalized message
    // private Map<String, String> getData()
    // {
    //     Map<String, String> data = new HashMap<String, String>();
    //     data.put("username", "Guest");
    //     data.put("message",  "Welcome to my world !!");
    //     return data;
    // }
}
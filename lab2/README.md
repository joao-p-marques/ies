* Para correr o servidor TomCat, definir a variavel $CATALINA_HOME e correr o ficheiro ./bin/startup.sh

* Criar um projeto maven, com a base de webapp, que vai criar um ficheiro .war
Esse ficheiro pode ser usado para lançar um servidor tomcat.

* Usar a interface gráfica do TomCat(em localhost:8080/manager) ou usar um IDE (extensão do VSCode para TomCat) que permite definir um servidor TomCat a usar, e lançar o ficheiro .war nesse servidor.

* Para criar um pequeno servlet, criar uma classe que faz uso dos HTTPRequests que sao enviados para o servidor

* O nome pode ser passado como parametro (http://localhost:8080/webapp-test-1.0-SNAPSHOT/NameServlet?username=ola) e o "username" vai ser apresentado.

* Um Servlet é responsável por lidar com os pedidos Http feitos ao servidor e prover uma interface para a resposta a esses pedidos, neste caso, com Java.

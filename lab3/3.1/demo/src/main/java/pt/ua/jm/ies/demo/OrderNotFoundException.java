package pt.ua.jm.ies.demo;

class OrderNotFoundException extends RuntimeException {

  OrderNotFoundException(Long id) {
    super("Could not find Order " + id);
  }
}
package pt.ua.jm.ies.demo;

enum Status {

    IN_PROGRESS,
    COMPLETED,
    CANCELLED;
  }
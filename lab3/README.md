
Employee is the base entity that is going to be used in our API.

This class doesn't need getters and setters as those methods are implemented in the API when we declare the class as an @Entity and as @Data by Lombok.
(as explained in the docs: @Data is a Lombok annotation to create all the getters, setters, equals, hash, and toString methods, based on the fields.)

Para implementar a persistencia dos dados, poderiamos, em vez de usar o modelo Spring Boot JPA com H2, usá-lo juntamente com um módulo de base de dados diferente. O H2 armazena os dados em memória, pelo que, quando o processo é fechado, os dados desaparecem. 

@Table, @Colum, @Id mapeiam campos da entidade Employee para tabelas SQL da base de dados

@Autowired permite que os métodos sejam criados diretamente a partir dos dados de execução

## Build

`mvn package`

## Run

`mvn clean spring-boot:run`
